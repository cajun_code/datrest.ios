//
//  RestClient.h
//  Created by Allan Davis on 10/3/13.
// Copyright (c) 2014 Cajun Code Software
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <AFNetworking/AFNetworking.h>

typedef void(^ResponseBlock)(NSURLResponse *response, id responseObject, NSError *error);

typedef NSURLRequest* (^HeaderBlock)(NSMutableURLRequest *request);

extern NSString * const HTTP_RESPONSE_UNAUTHORIZED;

@interface RestClient: NSObject


-(void) setDefaultHeaders:(HeaderBlock)header;

+ (id)sharedInstance;

@property (strong, nonatomic)NSURL *baseURL;

-(void) setBaseURLFromString:(NSString *)baseURLString;

@property BOOL useJSON;

//-(void) postPath:(NSString *)path
//      parameters:(NSDictionary *)parameters
//        formdata:(void ( ^ ) ( id<AFMultipartFormData> formData )) fileData
//         success:(void (^)(AFHTTPRequestOperation *, id))success
//         failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure;




-(void) getPath:(NSString *)path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler;

-(void) postPath:(NSString *)path
      parameters:(NSDictionary *)parameters
andCompletionHandler:(ResponseBlock) handler;

-(void) putPath:(NSString *) path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler;

-(void) patchPath:(NSString *) path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler;

-(void) deletePath:(NSString *) path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler;

-(NSDictionary *) addParamsToQueryString:(NSDictionary *) params;

-(void) clearHeaders;

-(void) addProtocol:(id)protocol;
-(void) removeProtocol:(id) protocol;

@end
