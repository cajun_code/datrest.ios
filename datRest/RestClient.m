//
//  RestClient.m
//  Created by Allan Davis on 10/3/13.
// Copyright (c) 2014 Cajun Code Software
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "RestClient.h"
//#import "ConfigHelper.h"
#import "Reachability.h"
//#import "Session.h"
#import <AFNetworking/AFNetworking.h>


NSString * const HTTP_RESPONSE_UNAUTHORIZED = @"com.cajuncode.datRest.unauthorized";

@interface RestClient (){
    HeaderBlock _header;
}


@property (strong, nonatomic) AFURLSessionManager *manager;


@end



@implementation RestClient

-(void) setDefaultHeaders:(HeaderBlock)header{
    _header = header;
}

+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}


-(id) init{
    self = [super init];
    if(self){
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        self.useJSON = NO;
        //self.manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
        [self.manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition (NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential * __autoreleasing *credential) {
            
            if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
                if([challenge.protectionSpace.host isEqualToString:@"tripfilesqaapi.cloudapp.net"]){
                    *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                    
                    return NSURLSessionAuthChallengeUseCredential;//completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
                }
            }
            
            return NSURLSessionAuthChallengePerformDefaultHandling;
        }];
    }
    return self;
}



-(void) setBaseURLFromString:(NSString *)baseURLString{
    self.baseURL = [NSURL URLWithString:baseURLString];
}

- (BOOL)serverIsNotReachable {
    Reachability *r = [Reachability reachabilityWithHostname:self.baseURL.host];
    NetworkStatus serverReachabilityStatus = r.currentReachabilityStatus;
    if (serverReachabilityStatus == NotReachable) {
        return YES;
    }
    return NO;
}

- (BOOL)deviceDoesNotHaveNetworkConnection {
    Reachability *r = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkReachabilityStatus = r.currentReachabilityStatus;
    if (networkReachabilityStatus == NotReachable) {
        return YES;
    }
    return NO;
}
    
-(BOOL) validateReachibilityWithFalure:(ResponseBlock)failure{
    BOOL reachable = !([self serverIsNotReachable]);
    BOOL connected = !([self deviceDoesNotHaveNetworkConnection]);
    if (reachable || connected) {
        return YES;
    }else{
        NSString *domain = nil;
        NSInteger code = -1;
        NSMutableDictionary *info = @{}.mutableCopy;
        if (!reachable) {
            info[@"message"] = @"The Server is not reachable";
            domain = @"REACHABILITYERROR";
            code = 201310071;
        }else{
            info[@"message"] = @"The Device is not Connected";
            domain = @"NONETWORKERROR";
            code = 201310072;
        }
        NSError * error = [NSError errorWithDomain:domain code:code userInfo:info];
        
        failure( nil, nil, error);
        return NO;
        
    }
}

    


-(NSDictionary *) addParamsToQueryString:(NSDictionary *) params{
    NSMutableDictionary * data = params.mutableCopy;
    
    
    return data;
}
-(void) getPath:(NSString *)path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler {
    
    [self processRequest:@"GET" toURLPath:path withParameters:parameters andCompletionHandler:handler];
    
}



-(void) postPath:(NSString *)path
      parameters:(NSDictionary *)parameters
         andCompletionHandler:(ResponseBlock) handler{
    
    [self processRequest:@"POST" toURLPath:path withParameters:parameters andCompletionHandler:handler];
    
}

-(void) putPath:(NSString *)path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler {
    
    [self processRequest:@"PUT" toURLPath:path withParameters:parameters andCompletionHandler:handler];
    
}
-(void) patchPath:(NSString *)path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler {
    
    [self processRequest:@"PATCH" toURLPath:path withParameters:parameters andCompletionHandler:handler];
    
}

-(void) deletePath:(NSString *)path parameters:(NSDictionary *)parameters andCompletionHandler:(ResponseBlock) handler {
    
    [self processRequest:@"DELETE" toURLPath:path withParameters:parameters andCompletionHandler:handler];
    
}

-(void) processRequest:(NSString *)mode toURLPath:(NSString*)path withParameters:(NSDictionary *)parameters
  andCompletionHandler:(ResponseBlock) handler{
    if ([self validateReachibilityWithFalure:handler]) {
        
        
        NSURL *url = [NSURL URLWithString:path relativeToURL:self.baseURL];
        NSError *error = nil;
        NSMutableURLRequest * request = nil;
        
        if (self.useJSON) {
            request = [[AFJSONRequestSerializer serializer] requestWithMethod:mode URLString:url.absoluteString parameters:parameters error:&error];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        }else{
            request = [[AFHTTPRequestSerializer serializer] requestWithMethod:mode URLString:url.absoluteString parameters:parameters error:&error];
        }
        if (error) {
            handler(nil, nil, error);
            return;
        }
        if(_header)
            _header(request);
        
        NSURLSessionDataTask *dataTask = [self.manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error){
            NSHTTPURLResponse * res = (NSHTTPURLResponse*)response;
            if (res && res.statusCode == 401) {
                NSNotification *note = [NSNotification notificationWithName:HTTP_RESPONSE_UNAUTHORIZED object:self];
                [[NSNotificationCenter defaultCenter] postNotification:note];
            }
            handler(response, responseObject, error);
        }];
        
        [dataTask resume];
    }

}

//
//// multi-part post with form data
//
//-(void) postPath:(NSString *)path
//      parameters:(NSDictionary *)parameters
//        formdata:(void ( ^ ) ( id<AFMultipartFormData> formData )) fileData
//         success:(void (^)(AFHTTPRequestOperation *, id))success
//         failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure{
//    
//    if ([self validateReachibilityWithFalure:failure]) {
//        
//        [self setHeaders];
//        
//        NSMutableURLRequest * request = [self multipartFormRequestWithMethod:@"POST" path:path parameters:[self addParamsToQueryString:parameters] constructingBodyWithBlock:fileData];
//        
//        AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
//        [self enqueueHTTPRequestOperation:operation];
//    }
//    
//}


//-(void) postFile:(NSData *)fileWithPath

-(void) clearHeaders{
    
}

-(void) addProtocol:(id)protocol{
    NSMutableArray *protocols = self.manager.session.configuration.protocolClasses.mutableCopy;
    if (!protocols) {
        protocols = @[].mutableCopy;
    }
    [protocols addObject:protocol];
    self.manager.session.configuration.protocolClasses = protocols;
}

-(void) removeProtocol:(id) protocol{
    NSMutableArray *protocols = self.manager.session.configuration.protocolClasses.mutableCopy;
    if (protocols) {
        [protocols removeObject:protocol];
    }
    self.manager.session.configuration.protocolClasses = protocols;
}

@end
