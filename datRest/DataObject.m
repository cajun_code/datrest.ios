//
//  DataObject.m
//
//  Created by Allan Davis on 10/7/13.
// Copyright (c) 2014 Cajun Code Software
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "DataObject.h"
#import <objc/runtime.h>
#import "NSString+Conversions.h"

@interface DataObject (){
    NSMutableDictionary *dataMapping;
}


@end

@implementation DataObject

-(NSString *)getPrimaryKeyName{
    return [NSString stringWithFormat:@"%@Id", NSStringFromClass(self.class)];
}

+(void)registerType:(id) type forCollection:(NSString *) collectionName{
    if (!collectionMapping) {
        collectionMapping = @{}.mutableCopy;
    }
    collectionMapping[collectionName] = type;
    
}

static NSMutableDictionary *collectionMapping;

-(NSString *) dateFormat{
    return @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'";
}
-(NSString *) stringFromDate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[self dateFormat]];
    return [formatter stringFromDate:date]
    ;
}
-(NSDate *) dateFromString:(NSString *)string{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[self dateFormat]];
    return [formatter dateFromString:string];
}
    
-(void) fromJSON:(id) data{
    id json = nil;
    if ([data isKindOfClass:[NSData class]]) {
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }else{
        json = data;
    }
    [self fromDictionary:json];
}
-(NSString *) checkKey:(NSString *)key aginstList:(NSArray *)keys{
    NSString *mapKey = nil;
    if ([keys containsObject:[key capitalizedString]]) {
        mapKey = [key capitalizedString];
    }
    if ([keys containsObject:[NSString stringToCapitalCamelCase:key]]) {
        mapKey = [NSString stringToCapitalCamelCase:key];
    }
    if ([keys containsObject:[NSString stringToUnderscores:key]]) {
        mapKey = [NSString stringToUnderscores:key];
    }
    if ([keys containsObject:[NSString stringToCamelCase:key]]) {
        mapKey = [NSString stringToCamelCase:key];
    }
    return mapKey;
}
-(void) fromDictionary:(NSDictionary *) data{
    if (!dataMapping) {
        dataMapping = @{}.mutableCopy;
    }
    
    
    NSDictionary *keys = [self getKeys];
    for (NSString *key in keys.allKeys) {
        id value = nil;
        NSString *mapKey = nil;
        if ([key isEqualToString:@"objectId"]) {
            mapKey = [self checkKey:[self getPrimaryKeyName] aginstList:data.allKeys];
            if (!mapKey) {
                mapKey = [self checkKey:@"id" aginstList:data.allKeys];
				if (!mapKey) {
					mapKey = [self checkKey:@"objectId" aginstList:data.allKeys];
				}
            }
        }else{
            mapKey = [self checkKey:key aginstList:data.allKeys];
        }
        if (mapKey) {
            dataMapping[key] = mapKey;
            value = [data objectForKey:mapKey];
        }
        if (value) {
            NSString *type = keys[key];
            if ([type isEqualToString:@"int"]  && [value isKindOfClass:[NSNull class]]){
                value = @(-1);
            }else{
                if ([value isKindOfClass:[NSNull class]]) {
                    value = nil;
                }
            }
            if ([type isEqualToString:@"NSDate"]) {
                if (![value isKindOfClass:[NSNull class]]) {
                    if ([value isKindOfClass:[NSString class]]) {
                        value = [self dateFromString:value];
                        
                    }else{
                        NSTimeInterval val = [value intValue];
                        value = [NSDate dateWithTimeIntervalSince1970:val];
                    }
                }else{
                    value = nil;
                }
            }
            Class klass = NSClassFromString(type);
            if ([klass isSubclassOfClass:[DataObject class]]) {
                DataObject *data = [[klass alloc] init];
                [data fromDictionary:value];
                value = data;
            }
            if([klass isSubclassOfClass:[NSArray class]]){
                NSMutableArray *data = @[].mutableCopy;
                NSArray *inboundData = value;
                Class dataClass = collectionMapping[key];
                if (dataClass) {
                    if ([dataClass isSubclassOfClass:[DataObject class]]) {
                        for (NSDictionary *item in inboundData) {
                            DataObject *instance = [[dataClass alloc] init];
                            [instance fromDictionary:item];
                            [data addObject:instance];
                        }
                    }
                    value = data;
                }
                
            }
            [self setValue:value forKey:key];
        }
    }

}

-(NSDictionary *) getKeys{
    NSMutableDictionary *keys = @{}.mutableCopy;
    unsigned int outCount, i;
    Class consideredClass = [self class];
    while (![consideredClass isEqual:[NSObject class]]) {
        objc_property_t *properties = class_copyPropertyList(consideredClass, &outCount);
        for(i = 0; i < outCount; i++) {
            objc_property_t property = properties[i];
            const char *propName = property_getName(property);
            if(propName) {
                NSString *propertyName = [NSString stringWithUTF8String:propName];
                NSString *propertyType = [self getPropertyType: [NSString stringWithUTF8String:property_getAttributes(property)]];
                //NSLog(@"Property Name: %@, Type: %@", propertyName, propertyType);
                keys[propertyName] = propertyType;
            }
        }
        free(properties);
        consideredClass = [consideredClass superclass];
    }
    return keys;
}


-(NSString *) getPropertyType:(NSString *)propAttributeString{
    unsigned long startingLocation = [propAttributeString rangeOfString:@"T"].location;
    unsigned long stopingLocation = [propAttributeString rangeOfString:@","].location;
    NSString *type = nil;
    if (stopingLocation == NSNotFound) {
        type = propAttributeString;
    }else{
        type = [propAttributeString substringWithRange:(NSRange){startingLocation, stopingLocation}];
    }
    char c1 = [type characterAtIndex:1];
    NSString *retType = nil;
    switch (c1) {
        case '@':
            retType = [type substringWithRange:(NSRange){3, [type rangeOfString:@"\"" options:NSBackwardsSearch].location - 3}];
            break;
        case 'i':
            retType = @"int";
            break;
        case 'B':
            retType = @"bool";
            break;
        case 'l':
            retType = @"long";
            break;
        case 'q':
            retType = @"long";
            break;
        case 'I':
            retType = @"long";
            break;
        case 'L':
            retType = @"long";
            break;
        case 'Q':
            retType = @"long";
            break;
        case 'S':
            retType = @"short";
            break;
        case 's':
            retType = @"short";
            break;
        case 'f':
            retType = @"float";
            break;
        case 'd':
            retType = @"double";
            break;
        case 'c':
            retType = @"char";
            break;
        case 'C':
            retType = @"char";
            break;
        case '*':
            retType = @"CString";
            break;
        case '{':
            retType = @"struct";
            break;
        default:
            retType = @"Unknown";
            break;
    }
    return retType;
    
}

-(NSData *) toJSON{
    NSDictionary *data = [self toDictionary];
    NSError * error = nil;
    
    NSData * value = [NSJSONSerialization dataWithJSONObject:data
                                                     options:NSJSONWritingPrettyPrinted
                                                       error:&error];
    if (!error) {
        return value;
    }else{
        return nil;
    }
    
}
-(NSDictionary *) toDictionary{
    NSMutableDictionary * data = @{}.mutableCopy;
    NSDictionary *keys = [self getKeys];
    for (NSString *key in [keys allKeys] ) {
        if ([key isEqualToString:@"superclass"]) {
            continue;
        }
        NSString *type = keys[key];
        id value = [self valueForKey:key];
        NSString *data_key = dataMapping[key];
        if(!data_key)
            data_key = key;
        if (value) {
            if ([type isEqualToString:@"NSDate"]) {
                value = [self stringFromDate:value];
            }
            Class klass = NSClassFromString(type);
            if ([klass isSubclassOfClass:[DataObject class]]) {
                DataObject *instance = value;
                value = [instance toDictionary];
            }
            if([klass isSubclassOfClass:[NSArray class]]){
                NSMutableArray *data = @[].mutableCopy;
                NSArray *inboundData = value;
                Class dataClass = collectionMapping[key];
                if (dataClass) {
                    if ([dataClass isSubclassOfClass:[DataObject class]]) {
                        for (DataObject *instance in inboundData) {
                            [data addObject:[instance toDictionary]];
                        }
                    }
                    value = data;
                }
                
            }

            [data setObject:value forKey:data_key];
        }else{
            [data setObject:[NSNull null] forKey:data_key];
        }
    }
    if ([data.allKeys containsObject:@"objectId"]) {
        NSString *key = [self getPrimaryKeyName];
        if (key) {
            data[key] = data[@"objectId"];
            [data removeObjectForKey:@"objectId"];
        }
    }
    
    return data;
}

-(void) saveWithCompletion:(void(^)(NSURLResponse* response, NSError* error))completion{
    NSString *url = nil;
    if (self.objectId) {
        url=[NSString stringWithFormat:@"%@/%@/", [self getURL], self.objectId];
    }else{
        url = [NSString stringWithFormat:@"%@/", [self getURL]];
    }
    RestClient *client = [RestClient sharedInstance];
    
    NSMutableDictionary * params = [self toDictionary].mutableCopy;
    
    [client postPath:url parameters:params andCompletionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
            if (!error) {
                if (response && [(NSHTTPURLResponse *)response statusCode] == 200){
                    [self fromJSON:responseObject];
                    completion(response,nil);
                    
                }else{
                    id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                    error = [[NSError alloc] initWithDomain:@"com.backpeddle.requesterror" code:[(NSHTTPURLResponse *)response statusCode] userInfo:@{NSLocalizedDescriptionKey: json[@"error"]}];
                    completion(response,error);
                }
                
            }else{
                completion(response,error);
            }

    }];
}

-(NSString *) generateJSONString{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.toDictionary
                                                       options:(NSJSONWritingOptions) 0
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

-(void) deleteWithCompletion:(void (^)(NSURLResponse *, NSError *))completion{
    NSString *url = nil;
    if (self.objectId) {
        url=[NSString stringWithFormat:@"%@/%@/", [self getURL], self.objectId];
    }else{
        url = [NSString stringWithFormat:@"%@/", [self getURL]];
    }
    RestClient *client = [RestClient sharedInstance];
    
    [client deletePath:url parameters:nil andCompletionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (!error) {
            if (response && [(NSHTTPURLResponse *)response statusCode] == 200){
                completion(response, error);
                
            }else{
                id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                error = [[NSError alloc] initWithDomain:@"com.backpeddle.requesterror" code:[(NSHTTPURLResponse *)response statusCode] userInfo:@{NSLocalizedDescriptionKey: json[@"error"]}];
                 completion(response, error);
            }
            
        }else{
             completion(nil, error);
        }
        
    }];

}

+(void) fetchWithId:(NSString *) object_id withCompletion:(CompletionHandler) handler{
    __block DataObject* data = [[self class] new];
    
    NSString *url=[NSString stringWithFormat:@"%@/%@/", [data getURL], object_id];
    
    RestClient *client = [RestClient sharedInstance];
    client.useJSON = YES;
    [client getPath:url parameters:nil andCompletionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            if (response && [(NSHTTPURLResponse *)response statusCode] == 200){
                [data fromJSON:responseObject];
				data.objectId = object_id;
                handler(data, response, error);
                
            }else{
                id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                error = [[NSError alloc] initWithDomain:@"com.backpeddle.requesterror" code:[(NSHTTPURLResponse *)response statusCode] userInfo:@{NSLocalizedDescriptionKey: json[@"error"]}];
                handler(data, response, error);
            }
            
        }else{
            handler(nil, response, error);
        }
        
        
        
    }];
}


-(NSString *)getURL{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return nil;
    
}

-(NSString *) getKeyName{
	@throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return nil;
}


@end
