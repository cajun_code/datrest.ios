//
//  DataObject.h
//
//  Created by Allan Davis on 10/7/13.
// Copyright (c) 2014 Cajun Code Software
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "RestClient.h"


@protocol DataObject <NSObject>

-(NSString *) getURL;
-(NSString *) getPrimaryKeyName;

@end

typedef void(^CompletionHandler)(id dataObject, NSURLResponse *response, NSError *error);

@interface DataObject : NSObject<DataObject>

+(void)registerType:(id) type forCollection:(NSString *) collectionName;

@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) NSMutableDictionary *mapping;

-(NSDictionary *)getKeys;

-(NSString *) dateFormat;
-(NSString *) stringFromDate:(NSDate *)date;
-(NSDate *) dateFromString:(NSString *)string;

-(void) fromJSON:(id) data;
-(void) fromDictionary:(NSDictionary *) data;
-(NSString *) generateJSONString;
    
-(NSString *) toJSON;
-(NSDictionary *) toDictionary;

+(NSArray *) listAll;

+(void) fetchWithId:(NSString *) object_id withCompletion:(CompletionHandler) handler;
-(void) saveWithCompletion:(void(^)(NSURLResponse* response, NSError* error))completion;
-(void) deleteWithCompletion:(void(^)(NSURLResponse* response, NSError* error))completion;

@end
