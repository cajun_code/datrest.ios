
Pod::Spec.new do |s|
  s.name         = "datRest"
  s.version      = "0.1.6"
  s.summary      = "Rest Client and Object Mapping System"
  s.homepage     = "https://bitbucket.org/cajun_code/datrest.ios"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.authors      = { "Allan Davis" => "cajun.code@gmail.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/cajun_code/datrest.ios.git", :tag => "v0.1.6" }
  s.source_files  = "datRest/**/*.{h,m}"
  s.requires_arc = true
  s.dependency "AFNetworking", "~> 2.2.0"
  s.dependency "Reachability", "~> 3.1.1"
  s.dependency "MBProgressHUD", "~> 0.8"
#  s.subspec "NetworkingTests" do |g|
#    g.frameworks = "XCTest"
#    g.source_files = "datRestTests/helpers/**/*.{h,m}",
#    g. prefix_header_contents = "#import <XCTest/XCTest.h>\n#import \"XCTest+Networked.h\""
#
#  end

end
