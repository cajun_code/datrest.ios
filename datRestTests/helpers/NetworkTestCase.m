//
//  NetworkTestCase.m
//  BackPeddle
//
//  Created by Allan Davis on 10/11/13.
//  Copyright (c) 2013 BackPeddle. All rights reserved.
//

#import "NetworkTestCase.h"


@implementation NetworkTestCase

+(NSData *) contentsForResource:(NSString *) resourceName{
    return [[NSData alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:resourceName ofType:@"json"]];
}

+ (BOOL)waitForCompletion:(NSTimeInterval)timeoutSecs
{
    NSDate	*timeoutDate = [NSDate dateWithTimeIntervalSinceNow:timeoutSecs];
    bool done = false;
    do {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.001]];
        if([timeoutDate timeIntervalSinceNow] < 0.0)
            break;
    } while (!done);
    
    return done;
}

+ (void)setUpNetworking
{
    
    // Put setup code here; it will be run once, before the first test case.
    [NSURLProtocol registerClass:[CannedURLProtocol class]];
}

+ (void)tearDownNetworking
{
    // Put teardown code here; it will be run once, after the last test case.
    [NSURLProtocol unregisterClass:[CannedURLProtocol class]];
    
}

+(void)createResponseFromFile:(NSString *)fileName andResponseCode:(NSInteger)responseCode {
    NSData * data = [self contentsForResource:fileName];
    [CannedURLProtocol setCannedResponseData:data];
    [CannedURLProtocol setCannedStatusCode:responseCode];
}
@end
