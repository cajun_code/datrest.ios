//
//  NetworkTestCase.h
//  BackPeddle
//
//  Created by Allan Davis on 10/11/13.
//  Copyright (c) 2013 BackPeddle. All rights reserved.
//


#import "CannedURLProtocol.h"

@interface NetworkTestCase : NSObject


+(void) setUpNetworking;
+(void) createResponseFromFile:(NSString*)fileName andResponseCode:(NSInteger) responseCode;
+ (BOOL)waitForCompletion:(NSTimeInterval)timeoutSecs;

+(void) tearDownNetworking;

@end
