//
//  CannedURLProtocol.h
//  BackPeddle
//
//  Created by Allan Davis on 10/10/13.
//  Copyright (c) 2013 BackPeddle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CannedURLProtocol : NSURLProtocol

+ (void) setCannedResponseData:(NSData *) data;
+ (void) setCannedHeaders:(NSDictionary *) headers;
+ (void) setCannedStatusCode:(NSInteger)statusCode;
+ (void) setCannedError:(NSError *)error;

@end
