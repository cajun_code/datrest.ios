//
//  CannedURLProtocol.m
//  BackPeddle
//
//  Created by Allan Davis on 10/10/13.
//  Copyright (c) 2013 BackPeddle. All rights reserved.
//

#import "CannedURLProtocol.h"

// Undocumented initializer obtained by class-dump - don't use this in production code destined for the App Store
@interface NSHTTPURLResponse(UndocumentedInitializer)
- (id)initWithURL:(NSURL*)URL statusCode:(NSInteger)statusCode headerFields:(NSDictionary*)headerFields requestTime:(double)requestTime;
@end


static NSData *gCannedResponseData = nil;
static NSDictionary *gCannedHeaders = nil;
static NSInteger gCannedStatusCode = 200;
static NSError *gCannedError = nil;

@implementation CannedURLProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
	// For now only supporting http GET
	return YES;//[[[request URL] scheme] isEqualToString:@"http"] && [[request HTTPMethod] isEqualToString:@"GET"];
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
	return request;
}

+ (void)setCannedResponseData:(NSData*)data {
	gCannedResponseData = data;
}

+ (void)setCannedHeaders:(NSDictionary*)headers {
	gCannedHeaders = headers;
}

+ (void)setCannedStatusCode:(NSInteger)statusCode {
	gCannedStatusCode = statusCode;
}

+ (void)setCannedError:(NSError*)error {
	gCannedError = error;
}

- (NSCachedURLResponse *)cachedResponse {
	return nil;
}

- (void)startLoading {
    NSURLRequest *request = [self request];
	id<NSURLProtocolClient> client = [self client];
	
	if(gCannedResponseData) {
		// Send the canned data
		NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[request URL]
																  statusCode:gCannedStatusCode
																headerFields:gCannedHeaders
																 requestTime:0.0];
		
		[client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
		[client URLProtocol:self didLoadData:gCannedResponseData];
		[client URLProtocolDidFinishLoading:self];
		
		response = nil;
	}
	else if(gCannedError) {
		// Send the canned error
		[client URLProtocol:self didFailWithError:gCannedError];
	}
}

- (void)stopLoading {
}

@end