//
//  datRestTests.m
//  datRestTests
//
//  Created by Allan Davis on 3/11/14.
//  Copyright (c) 2014 CajunCode Software. All rights reserved.
//

#import "Kiwi.h"

SPEC_BEGIN(DatRestSpec)
    describe(@"RestClient", ^{
        pending(@"should assemble url from baseurl and provided path", ^{});
        pending(@"should estblish authentication to webservices", ^{});
        pending(@"should set headers for request", ^{});
        pending(@"should execute a async get request", ^{});
        pending(@"should execute a async post request", ^{});
        pending(@"should execute a async post multipart data", ^{});
        pending(@"should execute a async put request", ^{});
        pending(@"should execute a async patch request", ^{});
        pending(@"should execute a async delete request", ^{});
    });
SPEC_END