
Pod::Spec.new do |s|
  s.name         = "datRestTestHelpers"
  s.version      = "0.0.8"
  s.summary      = "Rest Client and Object Mapping System Testing Helpers"
  s.homepage     = "https://bitbucket.org/cajun_code/datrest.ios"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.authors      = { "Allan Davis" => "cajun.code@gmail.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/cajun_code/datrest.ios.git", :tag => "v0.0.8" }
  s.requires_arc = true
  s.source_files = "datRestTests/helpers/**/*.{h,m}"
end
